package app

import (
	"gitlab.com/jayonq/bookstore_users_api/controllers/ping"
	"gitlab.com/jayonq/bookstore_users_api/controllers/users"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)
	router.GET("/users/:user_id", users.GetUser)
	router.POST("/users", users.CreateUser)

}
